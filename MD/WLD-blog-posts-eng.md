
id200

## Google's January 2020 Core Update: Has the Dust Settled?

>On January 13th, MozCast measured significant algorithm flux lasting about three days (the dotted line shows the 30-day average prior to the 13th, which is consistent with historical averages) ...

https://d1avok0lzls2w.cloudfront.net/uploads/blog/jan-2020-core-1-14979.png

>That same day, Google announced the release of a core update dubbed the January 2020 Core Update (in line with their recent naming conventions) ...

>On January 16th, Google announced the update was "mostly done," aligning fairly well with the measured temperatures in the graph above. Temperatures settled down after the three-day spike ...

>It appears that the dust has mostly settled on the January 2020 Core Update. Interpreting core updates can be challenging, but are there any takeaways we can gather from the data?

## Which verticals were hit hardest?

>MozCast is split into 20 verticals, matching Google AdWords categories. It can be tough to interpret single-day movement across categories, since they naturally vary, but here's the data for the range of the update (January 14–16) for the seven categories that topped 100°F on January 14 ...

https://d1avok0lzls2w.cloudfront.net/uploads/blog/jan-2020-core-5-19979.png

>Health tops the list, consistent with anecdotal evidence from previous core updates. One consistent finding, broadly speaking, is that sites impacted by one core update seem more likely to be impacted by subsequent core updates.
Who won and who lost this time?

>Winners/losers analyses can be dangerous, for a few reasons. First, they depend on your particular data set. Second, humans have a knack for seeing patterns that aren't there. It's easy to take a couple of data points and over-generalize. Third, there are many ways to measure changes over time.
>We can't entirely fix the first problem — that's the nature of data analysis. For the second problem, we have to trust you, the reader. We can partially address the third problem by making sure we're looking at changes both in absolute and relative terms. For example, knowing a site gained 100% SERP share isn't very interesting if it went from one ranking in our data set to two. So, for both of the following charts, we'll restrict our analysis to subdomains that had at least 25 rankings across MozCast's 10,000 SERPs on January 14th. We'll also display the raw ranking counts for some added perspective.

>Here are the top 25 winners by % change over the 3 days of the update. The "Jan 14" and "Jan 16" columns represent the total count of rankings (i.e. SERP share) on those days ...



[>source](https://moz.com/blog/googles-january-2020-core-update)



id201

## New Removals report in Search Console 

>We’re happy to announce that we’re launching a new version of the Removals report in Search Console, which enables site owners to temporarily hide a page from appearing in Google Search results. The new report also provides info on pages on your site that have been reported via other Google public tools. There are different tools available for you to report and remove information from Google, in this post we’ll focus on three areas that will be part of the new Search Console report: temporary removals, outdated content and SafeSearch filtering requests.

> Temporary removals
A temporary removal request is a way to remove specific content on your site from Google Search results. For example, if you have a URL that you need to take off Google Search quickly, you should use this tool. A successful request lasts about six months, which should be enough for you to find a permanent solution. You have two types of requests available:

>Temporary remove URL will hide the URL from Google Search results for about six months and clear the cached copy of the page. Clear cache URL clears the cached page and wipes out the page description snippet in Search results until the page is crawled again.

https://1.bp.blogspot.com/-3bx91SSRBKo/XjAFK7NSKyI/AAAAAAAAD5A/3dS-Y6X0NnQLFPY6L2RG-eT1rT2RMXkdgCLcBGAsYHQ/s1600/temporary-removals.png

https://1.bp.blogspot.com/-IIeojakdij4/XjAFPycG21I/AAAAAAAAD5E/iSXwxWbRjyUZXIHZtNUIeIbxTyvzQTrjQCLcBGAsYHQ/s1600/temporary-removals-request.png

[read more](https://webmasters.googleblog.com/2020/01/new-removals-report-in-search-console.html)


[>source – Webmaster Central Blog](https://webmasters.googleblog.com/2020/01/new-removals-report-in-search-console.html)








Adobe will finally kill Flash in 2020

https://cdn.vox-cdn.com/thumbor/wCHdiHlCYmgNaf3VcHx08OY6EKI=/0x0:600x445/920x613/filters:focal(252x175:348x271):format(webp)/cdn.vox-cdn.com/uploads/chorus_image/image/55875425/adobe-flash-logo.0.jpg

>Adobe is finally planning to kill off Flash once and for all. Chrome, Microsoft Edge, and Safari have all been blocking Flash over the past year, but Adobe is now planning to remove support for it fully by the end of 2020. “We will stop updating and distributing the Flash Player at the end of 2020 and encourage content creators to migrate any existing Flash content to these new open formats,” explains an Adobe spokesperson.

>A number of gaming, education, and video sites still use Flash, and Adobe says it remains committed to supporting the technology until 2020 alongside partners like Apple, Facebook, Google, Microsoft, and Mozilla. Microsoft says it plans to disable Flash by default in Edge and Internet Explorer in mid to late 2019, with a full removal from all supported versions of Windows by 2020. Google will continue phasing out Flash over the next few years, while Mozilla says Firefox users will be able to choose which websites are able to run Flash next month and allow Firefox Extended Support Release (ESR) users to keep using Flash until the end of 2020. Apple is also supportive of the 2020 end of life for Flash, and Safari currently requires explicit approval on each website even when Mac users opt to install Flash.

>2020 will mark an end of an era for Flash, but one that feels like it has been a long time coming. HTML5 standards have been implemented across all modern web browsers, and the need for Flash just isn’t there anymore. An end to Flash will bring with it obvious improvements in security and just pure battery life on laptops and other mobile devices that still support the web technology. 




[>source](https://www.theverge.com/2017/7/25/16026236/adobe-flash-end-of-support-2020)





Facebook is putting a surprising restriction on its independent oversight board

https://cdn.vox-cdn.com/thumbor/8z8B5xjsUsTkrpHtiSftnNURfYA=/0x0:2040x1360/920x613/filters:focal(857x517:1183x843):format(webp)/cdn.vox-cdn.com/uploads/chorus_image/image/66218741/akrales_180614_1777_0080.0.jpg

One of the year’s biggest stories at the intersection of technology and power is the Facebook Oversight Board. For the first time, a tech giant is seeking to give some of its power back to the people — in this case, in the form of an independent 40-member board that will serve as a kind of Supreme Court for content moderation. This week, Facebook announced that the board would likely begin hearing cases this summer — it also named the person who will lead the board’s staff, and released suggested bylaws.

So, what are people saying about the news?

At Wired, Steven Levy speculates that the board is likely to hear a case about Facebook’s policy against sending political ads out for fact-checking.

A complaint by a target of a bogus political ad is bound to come before the board eventually, which will certainly take on the case. Or Facebook itself might send the issue to the board. After all, this issue satisfies almost all the factors listed by Facebook itself when assessing important cases. (A subset of the board’s members will sit on a selection committee.) According to an explanation of the board’s charter written by Facebook, these include severity (“the content reaches or affects someone else’s voice, privacy or dignity”); public discourse (“the content spurs significant public debate and/or important political and social discourse”); and difficulty (“there is disagreement about Facebook’s decision on the content and/or the underlying policy or policies”). It’s almost as if the whole project was created specifically to rule on Zuckerberg’s stance on political ads.

Nick Clegg, the former UK deputy prime minister who is now Facebook’s head of global policy and communications, confirms this. “I think this political ads issue of how do you treat political speech is exactly the kind of thing where it would be very interesting to see how the board thinks,” he says.

I’m less certain the board will have a say here. It will have the authority to remove (or leave standing) individual pieces of content, as well as issue policy advisory opinions. Key word: advisory. And while an opinion by the board that Facebook should fact-check political ads would have some weight — and could provide political cover for Facebook to reverse course, should it decide it wants to — ultimately the decision will likely still remain with Zuckerberg.

At Lawfare, Evelyn Douek zooms in on one of the more peculiar features of the board, at least at launch: it will only review cases in which an individual believes their content was removed in error. If a post was allowed to stay up in error — a piece of viral misinformation about a health crisis, for example — the board will initially have no jurisdiction. (Facebook says that it will get such jurisdiction in the future but won’t specify a time frame.) Douek writes:

Limiting the board’s jurisdiction to take-down decisions stacks the deck somewhat. It is like introducing video appeals to tennis to make calls more accurate but allowing players a review only when balls are called “out” and not when a ball is called “in,” no matter how erroneous the call seems. For those in favor of longer rallies — which probably includes the broadcasters and advertisers — this is a win, because only those rallies cut short can be appealed. For those in favor of more accurate calls generally, not so much. Indeed, on a press call, Facebook made this bias toward leaving things up explicit: The limited ambit of operations to start is “due to the way our existing content moderation system works, and in line with Facebook’s commitment to free expression” (emphasis added). Maybe so, but it is a disappointing limitation and represents an uncharacteristically incremental approach from a company famous for “moving fast.” It is important to hold Facebook to its commitment that this will be changed in the near future.

It all feels rather like ... an oversight.

If Facebook expands the jurisdiction to include takedowns within the first few months of the board’s operation, I don’t think this omission is that big a deal. Much longer than that, though, and I’d say Facebook has a problem.

Then there’s the issue of how long Facebook might take to review a case. The wheels of justice aren’t known to spin particularly fast in any legitimate court, but it’s worth noting that this board has not been designed with rapid interventions in mind. Here’s Kurt Wagner at Bloomberg:

Facebook’s proposed Oversight Board, a group of people from outside the company who will determine whether controversial user posts violate the social network’s rules, could take months to make these decisions — indicating the panel won’t play a role in quickly stopping the viral spread of misinformation or abuse on the service.

 Instead, the board will take on cases that “guide Facebook’s future decisions and policies,” the company wrote Tuesday in a blog post. “We expect the board to come to a case decision, and for Facebook to have acted on that decision, in approximately 90 days.” The company also said it could expedite some decisions in “exceptional circumstances,” and that those would be completed within 30 days, but could be done faster.

Now, if you are a new mom and your photo of you breastfeeding your child has been removed in error, and you appeal and the board decides to hear your case as part of a landmark decision about global nipple viewability standards, you can probably stand to wait three months to have your answer. But if you are a business whose ads have been removed because they promote products that contain CBD oil, even though CBD oil is now widely legal, that three-month delay could mean the difference between life and death for your company.

Still, it’s worth noting that even this three-month process is far superior to the current system of justice, which involves filling out a form, sending it in, and praying. (The new system will also involve filling out a form, sending it in, and praying, but there is now a chance that an independent board will ask you to make your case more formally, consult with experts, and render a binding opinion in your favor.)

I’m glad that one of the world’s largest quasi-states has evolved to include a judicial system. It’s worth noting, though, that this system has been set up explicitly to redress the complaints of individual users. It won’t be asked to “fix Facebook” broadly — to make judgments in service of the health of the overall user base, or the world they inhabit. That remains at the sole discretion of the executive — Facebook’s CEO. And at a company where the CEO has majority control over voting shares, there is effectively no legislative branch.

Facebook is taking the boldest approach we’ve seen yet to establishing an independent mechanism of accountability for itself. But as the board prepares to name its members, it’s worth keeping our expectations in line with what they’ll actually be able to do.

[>source](https://www.theverge.com/interface/2020/1/30/21113273/facebook-oversight-board-jurisdiction-bylaws-restrictions)