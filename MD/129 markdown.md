## Лидер ли Laravel?

Зачастую разработчики с лёгкостью называют <a href="https://www.whitelabeldevelopers.ru/laravel">Laravel</a> самым популярным PHP-фреймворком в мире. Давайте с этим разберёмся. Для оценки возьмём оценку статистики общих поисковых запросов (web search) в тематике Internet&Telecom по основным PHP-фреймворкам: <a href="https://www.whitelabeldevelopers.ru/laravel">Laravel</a>, <a href="https://www.whitelabeldevelopers.ru/yii">Yii</a>, <a href="https://www.whitelabeldevelopers.ru/symphony">Symphony</a>, <a href="https://www.whitelabeldevelopers.ru/codeigniter">CodeIgniter</a> и <a href="https://www.whitelabeldevelopers.ru/nette">Nette</a>.

## Методика

Безусловно, [Google Trends](https://trends.google.com) нельзя воспринимать как точные данные, это просто общая оценка больших значений по которой можно лишь судить об тенденциях.


### Россия

![Статистика популярности PHP-фреймворков](upload/blog/images/id129/1.webp "Статистика популярности PHP-фреймворков")

У нас безоговорочным лидером является [Laravel](/laravel). На втором месте скорее [Symphony](/symphony), а [Yii](/yii) только третье.




### Украина

![Статистика популярности PHP-фреймворков](upload/blog/images/id129/2.webp "Статистика популярности PHP-фреймворков")

Без преувеличения соседская айти-держава отдаёт предпочтение [Laravel](/laravel) отдавая должное [Symphony](/symphony).



### Индия

![Статистика популярности PHP-фреймворков](upload/blog/images/id129/3.webp "Статистика популярности PHP-фреймворков")

Индия — единственная весомая айти-государство отдающее предпочтение [Codeigniter](/codeigniter), естествено, после [Laravel](/laravel).




### Китай

![1.webp](upload/blog/images/id129/4.webp "Статистика популярности PHP-фреймворков")

Поднебесная не принесла сюрпризов. [Laravel](/laravel), который прекрасно себя чувствует в связке с китайским [Vue.js](/vuejs) первый, [Yii](/yii) определённо побеждает [Symphony](/symphony). Предположу, что в Китае популярно всё, что хорошо для интернет-магазинов.
 


### Германия

![1.webp](upload/blog/images/id129/5.webp "Статистика популярности PHP-фреймворков")

Лидерство малоизвестного у нас чешского национального фреймворка <a href="https://www.whitelabeldevelopers.ru/nette">Nette</a> было предсказуемым. 




### Франция

![1.webp](upload/blog/images/id129/7.webp "Статистика популярности PHP-фреймворков")

Во Франции уникальная ситуация — [Symphony](/symphony) однозначный лидер. Надеюсь, что это не является следствием любви французов к опере. Чтобы исключить это у [Symphony](/symphony) стоит категория Software. Помимо этого, нужно учитывать, что это данные за 2019-й год, в котором очевидно, что большинство современного населения имеет интересы далёкие от классической музыки.





### Аргентина

![1.webp](upload/blog/images/id129/8.webp "8.webp")

[Laravel](/laravel) — лидер, [Symphony](/symphony) бьётся за второе место с [Codeigniter](/codeigniter) и побеждает.




### Бразилия

![1.webp](upload/blog/images/id129/9.webp "Статистика популярности PHP-фреймворков")

[Laravel](/laravel) — лидер,  [Codeigniter](/codeigniter) побеждает [Symphony](/symphony).





### СГА

![1.webp](upload/blog/images/id129/10.webp "Статистика популярности PHP-фреймворков")

Рынок заказной разработки СГА не просто самый крупный в мире, он, например, больше российского примерно в 5 раз. А российский по объёму соответствует голландскому. В целом именно северо-американский рынок определяет мировые тенденции в наше время. Рынок <a href="https://trends.google.com/trends/explore?geo=IT&q=Laravel,Yii,%2Fm%2F09cjcl,Nette,%2Fm%2F02qgdkj">Италии</a> аналогичен северо-американскому.




## Всемирные данные

![1.webp](upload/blog/images/id129/11.webp "Статистика популярности PHP-фреймворков")

[Laravel](/laravel) – безусловный лидер, [Symphony](/symphony) был бы уверенно вторым если бы не Индия, а для почитателей [Yii](/yii) у нас плохие новости – он интересен меньше [Nette](/nette).





<small> Автор  🕱 WhiteMan </small>